import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { User } from '../models/user';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';

@Injectable()
export class UserService {
    constructor(private http: HttpClient) { }

    getAll() {
        return this.http.get<User[]>(environment.host+'/users');
    }

    getById(id: number) {
        return this.http.get(environment.host+'/users/' + id);
    }
    register(user: User):Observable<any> {
        debugger
        return this.http.post<any>(environment.host+'/users/register', user);
    }

    update(user: User) {
        return this.http.put(environment.host+'/users/ '+ user.Id, user);
    }

    delete(id: number) {
        return this.http.delete(environment.host+'/users/' + id);
    }
}